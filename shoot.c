#include "sound.h"

/*PROTOTYPES FOR SHOOTING*/

struct shoot createBoom(struct coord parentCoord);

int updateShoots();

struct coord collDetBalls(struct coord initiator);

/*definitions*/

int shootnumber = 0;
int ball_width;
int ball_height;

struct shoot createBoom(struct coord parent){
	struct shoot child;
	child.ttl = TTL;
	child.x = parent.x + parent.w / 2;
	child.y = parent.y + parent.h / 2;
	child.w = ball_width;
	child.h = ball_height;
	child.direction = parent.direction;
	child.ID = parent.ID;
	return child;
}


int updateShoots(){
	if (shootnumber >= 0){
		int i;
		for(i = 0; i < shootnumber; i++){

			if(boom[i].ttl > 0){ 	/* boom[int] defined in gamedefs */	
				boom[i].ttl--;
				boom[i].x = boom[i].x - SPEED * 2 * sin(boom[i].direction * M_PI/180);
				boom[i].y = boom[i].y - SPEED * 2 * cos(boom[i].direction * M_PI/180);
				blitBall(boom[i].x, boom[i].y);
			}
			else{
				boom[i].x = SCREEN_WIDTH * 2;	/*boom is not longer on screen*/
				boom[i].y = SCREEN_HEIGHT * 2;
			}
		}
	}
	if (shootnumber < 0){
		shootnumber = 0; /* FIXME: check if needed */
		printf("FIXME\n");
	}
	if (shootnumber >= MAXSHOOTS - 2){	/*-2 is for security reasons.*/
		shootnumber = 0;		/*it can be, that both players */
	}					/*increase the shootnumber, so its more*/
}						/*safe this way.*/

struct coord collDetBalls(struct coord parent){
	if (shootnumber >= 0){
		int i;
		for(i = 0; i < shootnumber; i++){
			if(parent.x < boom[i].x){	/*this means, ball comes from right*/
				if(	(boom[i].x - parent.x) <= parent.w 
					&& (boom[i].y + boom[i].w - parent.y ) <= parent.w
					&& (boom[i].y - parent.y) >= 0 
					&& boom[i].ID != parent.ID){	
					parent.health -= 10;
					boom[i].ttl = 0;
					printf("Hit\n");
					alSourcePlay(Sources[SUCCES]);
					printf("Player %d has been hit.\n", parent.ID);
					printf("Player %d health: %d\n", parent.ID, parent.health);
				}
			}

			if(parent.x > boom[i].x){	/*this means, ball comes from left*/
				if(	(parent.x - boom[i].x) <= 30 
					&& (boom[i].y - parent.y) <= 30
					&& (boom[i].y - parent.y) >= 0 	
					&& boom[i].ID != parent.ID){	
					parent.health -= 10;
					boom[i].ttl = 0;
					printf("Hit\n");
					alSourcePlay(Sources[SUCCES]);
					printf("Player %d has been hit.\n", parent.ID);
					printf("Player %d health: %d\n", parent.ID, parent.health);
				}
			}
		}
	}
	return parent;
}

