#!/usr/bin/env bash

# 2020 Jan Peschke (jan@blabla.berlin)
# Mirrors an image and appends this one. 


infile="$1"
outfile="mirrored_$(basename -s .png ${infile}).png"

# mirror image
  convert -flop ${infile} mirror.png; 

# join images
convert +append ${infile} mirror.png ${outfile}

# cleanup
rm mirror.png; 
