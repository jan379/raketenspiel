#!/usr/bin/env bash

# 2020 Jan Peschke (jan@blabla.berlin)
# Takes a square image with suffix png,
# rotates it 36 times and joins all images
# in one large strip

# !!! I needed to adjust /etc/ImageMagick-6/policy.xml height/ width limits to get it working with bigger images !!! 

infile="$1"
outfile="stripped_$(basename -s .png ${infile}).png"

# rotate image
for i in $(seq -w 1 10 360 ); do 
  convert ${infile} -distort SRT $i rotated-$i.png; 
done

# join images
convert -limit width 10MP -limit height 10MP -limit memory 2GB +append "rotated*.png" ${outfile}

# cleanup
for i in $(seq -w 1 10 360 ); do 
  rm rotated-$i.png; 
done
