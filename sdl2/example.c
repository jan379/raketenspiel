// Example program:
// Using SDL2 to create an application window

// https://wiki.libsdl.org/MigrationGuide

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>

int main(int argc, char* argv[]) {
	SDL_Window *sdlWindow;
	SDL_Renderer *sdlRenderer;
	SDL_CreateWindowAndRenderer(0, 0, SDL_WINDOW_FULLSCREEN_DESKTOP, &sdlWindow, &sdlRenderer);
	if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO) != 0) {
		SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
		return 1;
	}

	// Check that the window was successfully created
	if (sdlWindow == NULL) {
		// In the case that the window could not be made...
		printf("Could not create window: %s\n", SDL_GetError());
		return 1;
	}
	// Initialize sdl_image
	// load support for the JPG and PNG image formats
	int flags=IMG_INIT_JPG|IMG_INIT_PNG;
	int initted=IMG_Init(flags);
	if((initted&flags) != flags) {
		printf("IMG_Init: Failed to init required jpg and png support!\n");
		printf("IMG_Init: %s\n", IMG_GetError());
		return 1;
	}

	// The window is open: could enter program loop here (see SDL_PollEvent())


	//  Try to load an image
	const char *image_path = "myimage.jpg";
	SDL_Surface *image = IMG_Load(image_path);

	/* Let the user know if the file failed to load */
	if (!image) {
		printf("Failed to load image at %s: %s\n", image_path, SDL_GetError());
		return 1;
	}
	/* Create a texture form surface */
	SDL_Texture *texture = SDL_CreateTextureFromSurface(sdlRenderer, image);
	/* After we created a Texture we release the surface resource */
	SDL_FreeSurface(image);

	SDL_RenderClear(sdlRenderer);
	SDL_RenderCopy(sdlRenderer, texture, NULL, NULL);
	SDL_RenderPresent(sdlRenderer);
	SDL_Delay(3000);  // Pause execution for 3000 milliseconds, for example

	// Close and destroy the window
	SDL_DestroyWindow(sdlWindow);

	// Clean up
	IMG_Quit();
	SDL_Quit();
	return 0;
}
