#ifndef GAMEDEFS_H
#define GAMEDEFS_H

#define  SCREEN_WIDTH  1024 
#define  SCREEN_HEIGHT  600
#define  MARGIN 30
#define  SPEED  10
#define  ACCELERATION  1
#define  MAX_SPEED  10
#define  MIN_SPEED  0


#define MAXSHOOTS 20
#define TTL 30 		/*time to live for every single shoot.*/

struct shoot{
	int ttl;	/*a shoot should not exist for ever*/
	int x;		
	int y;
	int w;		
	int h;
	int direction;
	int ID;		/*could be used to bind different properties to 
			  different players e.g. their shoots*/
};


struct coord{
        int x;
        int y;
	int w;
	int h;
	int speed;
        int key_left;
        int key_right;
        int key_up;
        int key_down;
        int key_shoot;
	int enginesound;
	float sound_x;
	float sound_y;
	int direction;
	int health;
	int points;
	int clipping;
	int ID;
	int shoot;
	int shootDelay;
};

/* let's move the world around */
struct world_coord{
	int x;
	int y;
};

struct shoot boom[MAXSHOOTS];


#endif 
