# Raketenspiel

(eigentlich gibt es gerade garkeine Rakete im Spiel, dafür aber zwei Astronauten).

![](Raketenspiel1.png)

## Install Dependencies:

On debian based systems, do a 

```
  apt-get install gcc libalut-dev libsdl-image1.2-dev
```

This will probably install every necessary dependency like openal-dev, sdl-dev and so on.

## How to compile:

```
gcc gameClient.c  -lopenal -lalut  -lSDL_image -lSDL -lm -o Shooter
```
