
/* Definitions */

int number_stars = 0;
int star_width;
int star_height;
SDL_Rect src, dest;

struct star createStar(struct coord parent){
	struct star child;
	child.ttl = TTL*10;
	child.x = parent.x - parent.w / 2;
	child.y = parent.y - parent.h / 2;
	child.w = star_width;
	child.h = star_height;
	child.ID = parent.ID;
	printf("A new star is born\n");
	if(number_stars < MAXSTARS - 1){
		number_stars++;
	} else {
		number_stars = 1;
	}
	return child;
}

int blitStar(SDL_Surface *screen, SDL_Surface *starsurface, int x, int y){
	SDL_SetAlpha(starsurface, SDL_SRCALPHA, 128);
	dest.w = starsurface->w; 
	dest.h = starsurface->h;
	dest.x = x;
	dest.y = y;
	SDL_BlitSurface(starsurface, 0, screen, &dest);
}

int updateStars(SDL_Surface *screen, SDL_Surface *starsurface){
	if (number_stars >= 1){
		int i;
		for(i = 1; i < MAXSTARS; i++){

			if(star[i].ttl > 0){ 	/* star[int] defined in stars.h */	
				star[i].ttl--;
				blitStar(screen, starsurface, star[i].x, star[i].y);
				printf("Star #%i at position %i and %i with TTL: %i\n", i, star[i].x, star[i].y, star[i].ttl);
			} else {
				star[i].x = -100;
				star[i].y = -100;
			}
		}
	}
}



