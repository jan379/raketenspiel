#include <stdio.h>
#include <math.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "gamedefs.h"
#include "shoot.c"
#include "stars.h"
#include "stars.c"
#include "sound.h"
#include "sound.c"

/*SDL stuff*/
SDL_Surface *screen, *player, *opponent, *ball, *twinkle, *health, *healthPlayer, *healthOpp, *planet;
SDL_Rect src, dest;
SDL_Event event;
Uint8 *keys;
int done = 0; /* switch for terminating the game loop */
int shootDelay = 0;
int source_state;
struct world_coord myworld;

struct coord playerCoord, opponentCoord; /* DEFINITION */

int graphicsInit(){

	printf("Initialize graphics\n");
	
	/*load graphics into buffer*/
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		printf("Kann SDL nicht initialisieren: %s\n", SDL_GetError());
		return 1;
	}

	/*Sicherstellen, dass SDL beendet wird bei Programmende.*/
	atexit(SDL_Quit);

	/*try to set videomode according to defined values at gamedefs.h at 8 bit. */
	//screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 8, SDL_HWSURFACE | SDL_FULLSCREEN);
	screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 16, SDL_HWSURFACE | SDL_FULLSCREEN);
	//screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 8, SDL_HWSURFACE);
	//screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 16, SDL_HWSURFACE);
	SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 0, 0,0));
	if (screen == NULL) {
		printf("Cannot set videomode: %s\n", SDL_GetError());
		return 1;
	}

	printf("loading image files...\n");

	player = IMG_Load("./graphics/player.png");
	if (player == NULL) {
		printf("Not able to load graphics for player.\n");
		return 1;
	}
	opponent = IMG_Load("./graphics/opponent.png");
	if (opponent == NULL) {
		printf("Not able to load graphics for opponent.\n");
		return 1;
	}
	ball = IMG_Load("./graphics/ball.png");
        if (ball == NULL) {
                printf("Not able to load bullet graphics.\n");
		return 1;
	}
	twinkle = IMG_Load("./graphics/star.png");
        if (star == NULL) {
                printf("Not able to load star graphics.\n");
		return 1;
	}
	planet = IMG_Load("./graphics/planet_earth.png");
        if (planet == NULL) {
                printf("Not able to load planet graphics.\n");
		return 1;
	}
	health = IMG_Load("./graphics/healthBar.png");
        if (health == NULL) {
                printf("Not able to load graphics for playerHealth.\n");
		return 1;
	}
	healthPlayer = IMG_Load("./graphics/healthPlayer.png");
        if (healthPlayer == NULL) {
                printf("Not able to load graphics for healthPlayer.\n");
		return 1;
	}
	healthOpp = IMG_Load("./graphics/healthOpponent.png");
        if (healthOpp == NULL) {
                printf("Not able to load graphics for healthOpponent.\n");
		return 1;
	}
}

struct coord playerDead(struct coord player){
	int dead = player.ID;
	printf("player %d died.\n", dead);
	star[number_stars] = createStar(player);
	return player;
}

int updateWorld(){
	while(myworld.x > SCREEN_WIDTH){
		myworld.x = 0 - planet->w;
	}
	while(myworld.x < 0 - planet->w){
		myworld.x = SCREEN_WIDTH;
	}
}

struct coord playerControl(struct coord temp){

	/*First attempt to insert collision detection*/
	temp = collDetBalls(temp);

	/*what happens if we die?*/
	if(temp.health <= 0){
		playerDead(temp);
		temp.health = 100;
	}

	/*update player sound*/
	temp.sound_x =  (1.0 * ((float)(temp.x) + (float)(temp.w) / 2.0) / SCREEN_WIDTH) - 0.5;
	temp.sound_y =  (1.0 * ((float)(temp.y) + (float)(temp.h) / 2.0) / SCREEN_HEIGHT) - 0.5;
	alSource3f(Sources[temp.enginesound], AL_POSITION, temp.sound_x , temp.sound_y, 1.0);
	if(temp.speed > MIN_SPEED){
		alGetSourcei(Sources[temp.enginesound], AL_SOURCE_STATE, &source_state);
		if(source_state != AL_PLAYING){
			printf("playing source %i\n", temp.enginesound);
			alSourcePlay(Sources[temp.enginesound]);
		}
	}
	/*react on button presses*/
	if(keys[temp.key_up]){
		if(temp.speed <= MAX_SPEED ){ 
			temp.speed += ACCELERATION;
		}
		if(temp.speed > MAX_SPEED){ 
			temp.speed = MAX_SPEED;
		}
	}
	if(keys[temp.key_down]){
		if(temp.speed >= MIN_SPEED){ 
			temp.speed -= ACCELERATION;
		}
		if(temp.speed < MIN_SPEED){ 
			temp.speed = 0;
		}
	}

	if(keys[temp.key_left]){
		temp.direction += 10;
		temp.clipping = temp.clipping - temp.w;
                if(temp.clipping > temp.w*35){
               		temp.clipping = 0;
                }
                if(temp.clipping < 0){
                	temp.clipping = temp.w*35;
                }
	}
	if(keys[temp.key_right]){
		temp.direction -= 10;
		temp.clipping = temp.clipping + temp.w;
                if(temp.clipping > temp.w*35){
                	temp.clipping = 0;
                }
                if(temp.clipping < 0){
                	temp.clipping = temp.w*35;
                }
	}	
	if (keys[temp.key_shoot]){ 
		if (temp.shootDelay == 1  && temp.shoot < MAXSHOOTS / 2 ){ 
			boom[shootnumber] = createBoom(temp);
			shootnumber++;
			temp.shoot++;
			alSourcePlay(Sources[RUMBLE1]);	
		}
		if (temp.shoot >= MAXSHOOTS / 2) 
			temp.shoot = 0;
		if (temp.shootDelay >= 10){
			temp.shootDelay = 0;
		}
		if (temp.shootDelay < 10){
			temp.shootDelay++;
		}
	
	} 
	else {
	/* shoot-button NOT pressed */
	shootDelay = 0;
	}

	/* calculate actual movement */
	while(temp.direction >= 360){
		temp.direction -= 360;
	}
	while(temp.direction < 1){
		temp.direction += 360;
	}
	printf("player %d direction: %d \n", temp.ID, temp.direction);
	printf("player %d speed: %d \n", temp.ID, temp.speed);
	/*follow the computed direction*/
	temp.x = temp.x - temp.speed * sin(temp.direction * M_PI/180);
	temp.y = temp.y - temp.speed * cos(temp.direction * M_PI/180);
	/*add some gravity:*/
	temp.y++;

	/*sanity checks: stay on screen*/
	while(temp.x < 0){
		myworld.x -= temp.x;
		temp.x = 0;
	}
	while(temp.x > SCREEN_WIDTH - temp.w){
		myworld.x -= temp.x + temp.w - SCREEN_WIDTH;
		//myworld.x--;
		temp.x = SCREEN_WIDTH - temp.w;
	}
	while(temp.y > SCREEN_HEIGHT - temp.h){
		temp.y = SCREEN_HEIGHT - temp.h;
	}
	while(temp.y < 0){
		temp.y = 0;
	}
	return temp;
}

int blitBall(int x, int y){
	SDL_SetAlpha(ball, SDL_SRCALPHA, 128);
	dest.w = ball->w; 
	dest.h = ball->h;
	dest.x = x;
	dest.y = y;
	SDL_BlitSurface(ball, 0, screen, &dest);
}

int blitPlanet(int x, int y){
	SDL_SetAlpha(planet, SDL_SRCALPHA, 128);
	dest.w = planet->w; 
	dest.h = planet->h;
	dest.x = x;
	dest.y = y;
	SDL_BlitSurface(planet, 0, screen, &dest);
	printf("planet is drawn at %d, %d.\n", x, y);
}

int blitHealth(struct coord parent, int x, int y){
	int clipping = (health->w / 100) * parent.health;
	SDL_SetAlpha(health, SDL_SRCALPHA, 128);
	src.w = clipping;
	src.h = health->h;
	src.y = 0;
	src.x = 0;
	dest.w = src.w;
	dest.h = src.h;
	dest.x = x;
	dest.y = y;
	SDL_BlitSurface(health, &src, screen, &dest);

	if (parent.ID == 0){
		SDL_SetAlpha(health, SDL_SRCALPHA, 128);
		dest.w = src.w;
		dest.h = src.h;
		dest.x = x;
		dest.y = y + health->h;
		SDL_BlitSurface(healthPlayer, NULL, screen, &dest);
	}
	if (parent.ID == 1){
		SDL_SetAlpha(health, SDL_SRCALPHA, 128);
		dest.w = src.w;
		dest.h = src.h;
		dest.x = x;
		dest.y = y + health->h;
		SDL_BlitSurface(healthOpp, NULL, screen, &dest);
	}

}

int blitControl(){

	/* draw player */
	SDL_SetAlpha(player, SDL_SRCALPHA, 128);
	src.w = player->w/36;
	src.h = player->h;
	src.y = 0;
	src.x = playerCoord.clipping;
	dest.w = src.w;
	dest.h = src.h;
	dest.x = playerCoord.x;
	dest.y = playerCoord.y;
	SDL_BlitSurface(player, &src, screen, &dest);

	/* draw opponent */
	SDL_SetAlpha(opponent, SDL_SRCALPHA, 128);
	src.w = opponent->w/36;
	src.h = opponent->h;
	src.y = 0;
	src.x = opponentCoord.clipping;
	dest.w = src.w;
	dest.h = src.h;
	dest.x = opponentCoord.x;
	dest.y = opponentCoord.y;
	SDL_BlitSurface(opponent, &src, screen, &dest);

	/* draw player health */
	blitHealth(playerCoord, SCREEN_WIDTH - MARGIN - health->w, MARGIN);

	/* draw opponent health */
	blitHealth(opponentCoord, 0 + MARGIN, MARGIN);

}
int main(){

graphicsInit();
SDL_WM_SetCaption("To the stars...", "It's not a shooter!");
soundInit();
SDL_ShowCursor(0);

playerCoord.w = player->w/36;
playerCoord.h = player->h;
playerCoord.x = 0 + playerCoord.w;
playerCoord.y = screen->h - playerCoord.h;
playerCoord.direction = 1;
playerCoord.ID = 0;
playerCoord.health = 100;
playerCoord.key_up = SDLK_UP;
playerCoord.key_down = SDLK_DOWN;
playerCoord.key_left = SDLK_LEFT;
playerCoord.key_right = SDLK_RIGHT;
playerCoord.key_shoot = SDLK_RCTRL;
playerCoord.enginesound = ENGINE1;

opponentCoord.w = opponent->w/36;
opponentCoord.h = opponent->h;
opponentCoord.x = screen->w - opponentCoord.w;
opponentCoord.y = screen->h - opponentCoord.h;
opponentCoord.direction = 0;
opponentCoord.ID = 1;
opponentCoord.health = 100;
opponentCoord.key_up = SDLK_w;
opponentCoord.key_down = SDLK_s;
opponentCoord.key_left = SDLK_a;
opponentCoord.key_right = SDLK_d;
opponentCoord.key_shoot = SDLK_SPACE;
opponentCoord.enginesound = ENGINE2;

ball_width = ball->w;
ball_height = ball->h;

star_width = star->w;
star_height = star->h;

myworld.x = 0;
myworld.y = 0;

printf("Mainloop starts now.\n");
while (!done) {

	while (SDL_PollEvent(&event)) {
        	switch(event.type) {
	        case SDL_QUIT:
		done = 1;
		break;
		}
	}
	keys = SDL_GetKeyState(NULL);
	SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 123, 123, 250));
	playerCoord = playerControl(playerCoord);
	opponentCoord = playerControl(opponentCoord);
	updateWorld();
	blitPlanet(myworld.x, myworld.y + SCREEN_HEIGHT - planet->h);
	updateShoots();
	blitControl();
	updateStars(screen, twinkle);


	if(keys[SDLK_ESCAPE]){
		done = 1;
	}


	/*SDl screen-update:*/

	SDL_UpdateRect(screen, 0, 0, 0, 0); 
	SDL_Delay(5);

} 	/*ending while-loop*/

/*Den fuer das Bild reservierten Speicher freigeben:*/
SDL_FreeSurface(player);
SDL_FreeSurface(opponent);
SDL_FreeSurface(ball);
SDL_FreeSurface(twinkle);
SDL_FreeSurface(planet);
SDL_FreeSurface(screen);
KillALData();
return 0;
}



