#ifndef SOUND_H
#define SOUND_H


#include <AL/al.h>
#include <AL/alc.h>
#include <AL/alut.h>
/*global things*/
#define NUM_BUFFERS 5	/*OpenAL sound Buffer*/
#define NUM_SOURCES 5	/*OpenAl Sound sources*/
#define RUMBLE2 0	/*numbered sounds used in game*/
#define RUMBLE1 1
#define SUCCES  2
#define ENGINE1 3
#define ENGINE2 4

/*Declarations of the game-intern sound functions*/

void SetListenerValues();

void KillALData();

int soundInit();

ALboolean LoadALData();
ALuint Buffers[NUM_BUFFERS];
ALuint Sources[NUM_SOURCES];
/* Position of the source sound.*/
ALfloat SourcesPos[NUM_SOURCES] [4];

/* Velocity of the source sound.*/
ALfloat SourcesVel[NUM_SOURCES] [4];
	
/* Position of the Listener.*/
ALfloat ListenerPos[] = { 0.0, 0.0, 0.0 };

/* Velocity of the Listener.*/
ALfloat ListenerVel[] = { 0.0, 0.0, 0.0 };

/* Orientation of the Listener. (first 3 elements are "at", second 3 are "up")
   Also note that these should be units of '1'.*/
ALfloat ListenerOri[] = { 0.0, 0.0, -3.0,  0.0, 1.0, 0.0 };
#endif
