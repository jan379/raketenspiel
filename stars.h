#ifndef STARS_H
#define STARS_H

/* globals */

#define MAXSTARS 20

/* Function declarations */
struct star createStar(struct coord parent);
int updateStars(SDL_Surface *screen, SDL_Surface *star);
int blitStar(SDL_Surface *screen, SDL_Surface *star, int x, int y);

/* data structures */
struct star{
	int ttl;	/*a star should not exist for ever*/
	int x;		
	int y;
	int w;		
	int h;
	int ID;		/*could be used to bind different properties to 
			  different players e.g. their stars*/
};

struct star star[MAXSTARS];

#endif 
